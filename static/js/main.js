function init_app()
{
    // Polling function (BAD IDEA!!!!!!!!!!)
    if (!document.getElementById("room").loaded || !document.getElementById("panel").loaded)
    {
        setTimeout(init_app, 100);
        return;
    }

    document.getElementById("room").userName = queryValue("userName");

    var roomId = queryValue("roomId");
    if (roomId != "")
        document.getElementById("room").roomId = roomId;
    else
        document.getElementById("panel").show();
}

function queryValue(name)
{
    return queryValueFor(window.location.search, name);
}

function queryValueFor(search, name)
{
    if (search === "" || typeof name !== "string" || name === "")
        return "";

    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(search);
    return (results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " ")));
}

function makeId(len)
{
    if (!Number.isInteger(len) || len < 1)
        len = 5;

    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}